<?php

	session_start();

	if(!isset($_SESSION['logged_in'])){
		if(!isset($_SESSION['admin_login']){
		header('Location: index.php');
		exit();
		}
	}

?>

<!doctype html>
<html lang="pl">
<head>
	<meta charset="utf-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1"/>
	<title>Kamienica main</title>
	<link rel="stylesheet" href="style.css" type = "text/css">
</head>
<body>

	<div id="container">

		<div id="topbar">

        <div class="shadow">
            <img src="img/flamenco.jpg" />
        </div>
        <br/>
			<?php
				echo "<div class='logout'><a href='logout.php'><img src='img/turnoff.png' height='50px'></a></div>";

				echo "<div class='usernumber'>Witaj użytkowniku mieszkania nr ".$_SESSION['user']." </div><br/>";
			?>

		</div>

		<div id="sidebar">
				<div class="optionL">
                    <ul>
                        <li><a href="#">kącik wstydu</a>
                            <ul>
                                <li><a href="#">ośmiecony trawnik</a></li>
                                <li><a href="#">kto zostawił ten wózek tutaj</a></li>
                                <li><a href="#">ale konto to ty usuń!</a></li>
                                <li><a href="#">prosze wytrzeć ten trawnik</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
		</div>

		<?php
			require_once 'Roman.php';

			echo "<div id='container2'>";
			echo "<br/><h3><b>۩ DANE NA TEMAT UŻYCIA ۩</b></h3><br/>";

            //................................................................................................................................funkcja tworzaca tabele
            function drawTable($picture, $resource)
    		{
				echo "<br/><table><th colspan = 13><img src='img/".$picture.".png' height='45px' width='70px'></th><tr>";

				echo "<td>miesiąc</td>";
                $year = (string) date("Y")-1;
                $month = (string) date("n")+1;

                for($i=1;$i<=12;$i++){

					echo "<td>".Numbers_Roman::toNumeral($month)."</td>";

                		if($month<=11){
                			$month ++;
                		}elseif($month==12){
                			$month=1;
                		}

                }



				echo "</tr><tr>";

                echo "<td>stan</td>";
				for($i=1; $i<13; $i++)
				{
					echo "<td>".$_SESSION[''.$resource.''.$i.'']."</td>";
				}


				echo "</tr></table>";
			}

			//.....................................................................................................................................woda ciepla
			echo "<b>Zużycie ciepłej wody [m<sup>3</sup>]</b>";



            drawTable("hot","hwater");

			//.....................................................................................................................................woda zimna
			echo "<b>Zużycie zimnej wody [m<sup>3</sup>]</b></b>";

	        drawTable("cold","cwater");

            //.....................................................................................................................................prunt
			echo "<b>Zużycie elektryczności [kWh]</b>";

			drawTable("thunder","electricity");

			echo "<br/><br/>";

			echo "<b>Kalkulacje</b>";	 //to trzeba bedzie ubrac w funkcje

			echo "<table class='koszty'><tr><td class='lewa'><b>cena metra sześciennego ciepłej wody</b>: </td><td>36.00 zł/m<sup>3</sup></td>";
			echo "<tr><td class='lewa'><b>cena metra sześciennego zimnej wody</b>: </td><td>11.50 zł/m<sup>3</sup> </td>";
			echo "<tr><td class='lewa'><b>cena kilowatogodziny prądu</b>: </td><td>0.77zł/kWh</td>";

			echo "<tr><td class='lewa'><b>zużycie ciepłej wody w tym miesiącu</b>: </td><td>".$_SESSION['hot_water_use']." m<sup>3</sup></td>";
			echo "<tr><td class='lewa'><b>zużycie zimnej wody w tym miesiącu</b>: </td><td>".$_SESSION['cold_water_use']." m<sup>3</sup></td>";
			echo "<tr><td class='lewa'><b>zużycie prądu w tym miesiącu</b>: </td><td>".$_SESSION['electricity_use']." kWh</td>";

			echo "<tr><td class='lewa'><b>Twój koszt zużycia ciepłej wody wynosi</b>: </td><td>".$_SESSION['hot_water_use']." m<sup>3</sup> * 36.00 zł/m<sup>3</sup> = ".$_SESSION['hot_water_cost']." zł</td>";
			echo "<tr><td class='lewa'><b>Twój koszt zużycia zimnej wody wynosi</b>: </td><td>".$_SESSION['cold_water_use']." m<sup>3</sup> * 11.50 zł/m<sup>3</sup> = ".$_SESSION['cold_water_cost']." zł</td>";
			echo "<tr><td class='lewa'><b>Twój koszt zużycia prądu wynosi</b>: </td><td>".$_SESSION['electricity_use']." kWh * 0.77 zł/kWh = ".$_SESSION['electricity_cost']." zł</td>";

			echo "<tr><td class='lewa'><b>Razem do zapłacenia</b>: </td><td>".$_SESSION['hot_water_cost']." zł + ".$_SESSION['cold_water_cost']." zł + ".$_SESSION['electricity_cost']." zł = ".$_SESSION['total']." zł"."</td></table>";


			echo "<p><b>Twój e-mail: </b>: ".$year."";

			echo "</div>";

		?>


	</div>

	<script src="jquery-1.11.3.min.js"></script>

	<script>

		$(document).ready(function() {
    		var NavY = $('#sidebar').offset().top;

    		var stickyNav = function()
            {
        		var ScrollY = $(window).scrollTop();

        		if (ScrollY > NavY) {
        			$('#sidebar').addClass('sticky');
    		}

            else
            {
        		$('#sidebar').removeClass('sticky');
    		}
		};

		stickyNav();

		$(window).scroll(function() {
			stickyNav();
			});
		});

	</script>

</body>


</html>
