<?php

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//TU WCHODZIMY TYLKO NA CHWILĘ, PRZERABIAMY DANE ZEBRANE OD UŻYTKOWNIKA I POBIERAMY DANE Z BAZY DANYCH, PO CZYM PRZEKIEROWUJEMY DO kamienica.php ALBO index.php
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!



	session_start();

	if( (!isset($_POST['login'])) || (!isset($_POST['haslo'])) ) {
		header('location:index.php');
		exit();
	}
	
	require_once("connect.php");

	$connection = new mysqli($host, $db_user, $db_password, $db_name);

	include_once("functions.php");
	
    
    
    
	
	if($connection->connect_errno!=0)
	{
		echo "Error: ".$connection->connect_errno;
	}	
	else
	{
        $login=$_POST['login']; //sciagamy login i haslo z index.php
    	$password=$_POST['haslo'];
        
        $login=htmlentities($login, ENT_QUOTES, "UTF-8"); //czyscimy login ze znakow specjalnych
        
		
        // bardzo ciekawy if -  w którym jest przypisanie, wiec on sie i tak zadzieje!
        $result=@$connection->query( // w rezultacie zapiszemy kwerende wybierajaca cały row
    	sprintf("SELECT * FROM uzytkownicy WHERE user='%s'",  // '%s' oznacza ze w to miejsce wskoczy zawartosc drugiego argumentu
		mysqli_real_escape_string($connection, $login)));
		
        if($result)	{
			$how_many_users=$result->num_rows; //liczymy ile rzedow zwrocila kwerenda wybierajaca po userze (czyli loginie) - domyslnie powinno to byc wiecej od 0, tak naprawde to 1:)
			if($how_many_users>0) {
               
				$row=$result->fetch_assoc(); //przypisuje do zmiennej $row zwrócony rząd jako tablice ze slotami ktore maja nazwy takie jak kolumny w tabeli >> $row jest teraz tablica
				
				if(password_verify($password, $row['pass'])){ // ta funkcja zwróci true jesli $haslo == zmienna z hashem - w tym wypadku $row['pass'] gdzie umiescilismy przehashowane wczesniej haslo..........................................................w komorce $row['pass'] siedzi hasło przehashowane - a ta funkcja bierze zmienna $haslo i tłumaczy je sobie a potem porównuje
				
				
					//LOGOWANIE ADMINA
                    if($login=='99'){
                        
                        $_SESSION['admin_login'] = $login; //$_POST['login'];
                        $_SESSION['admin_password'] = $password; //$_POST['haslo']; 
           
                        $_SESSION['logged_in']=true;
                        header('location:admin_area/index.php'); 
                        
                        exit();
                    }
                    
                    else{ //logowanie uzytkowników
                    
    					$_SESSION['logged_in']=true;
    					$_SESSION['id']=$row['id'];
    					$_SESSION['user']=$row['user'];
                        
    									
    					
    					get_usage('cwater');
    					get_usage('hwater');
                        get_usage('electricity');
                           
    			    	
                           
                        //tworzymy nowe zmienne sesyjne w które wkładamy wartosci z odpowiednich kolumn   
                           
    					$_SESSION['electricity'] = $_SESSION['electricity12']; //zuzycie z ostatniego miesiaca
    					$_SESSION['cold_water'] = $_SESSION['cwater12'];
    					$_SESSION['hot_water'] = $_SESSION['hwater12'];
                        
                        $_SESSION['electricity_prev'] = $_SESSION['electricity11']; //zuzycie z przedostatniego miesiaca
        				$_SESSION['cold_water_prev'] = $_SESSION['cwater11'];
    					$_SESSION['hot_water_prev'] = $_SESSION['hwater11'];
    					
    					if(($_SESSION['electricity']>0) && ($_SESSION['hot_water']>0) && ($_SESSION['cold_water']>0)){ // sprawdzamy i liczymy ostatni miesiac
    					
        					$_SESSION['electricity_use'] = $_SESSION['electricity'] - $_SESSION['electricity_prev']; //tworzymy nowe zmienne sesyjne w które wkładamy wynik odejmowanka - czyli zuzycie z ostatniego miesiaca
        					$_SESSION['hot_water_use'] = $_SESSION['hot_water'] - $_SESSION['hot_water_prev'];
        					$_SESSION['cold_water_use'] = $_SESSION['cold_water'] - $_SESSION['cold_water_prev'];			
        
                            
                            //wybieramy ceny surowców
                            
                            $_SESSION['ele_price'] = get_price('electricity');
                            $_SESSION['hwtr_price'] = get_price('hot_water');
                            $_SESSION['cwtr_price'] = get_price('cold_water');
                            
                            
        					$_SESSION['electricity_cost'] = $_SESSION['electricity_use']* $ele_price;//0.77; //kolejne zmienne sesyjne - tym razem obliczamy: mnozymy róznicę z bierzacego i poprzedniego miesiąca przed odpowiednią liczbe i wychodzi nam cena
        					$_SESSION['hot_water_cost'] = $_SESSION['hot_water_use']*$hwtr_price;//36.00;
        					$_SESSION['cold_water_cost'] = $_SESSION['cold_water_use']* $cwtr_price;//11.50;	
        
        					$_SESSION['total'] = $_SESSION['electricity_cost'] + $_SESSION['hot_water_cost'] + $_SESSION['cold_water_cost'];
    					
    					} 
                        else 
                        {
    						$_SESSION['electricity_use']='brak danych ';
    						$_SESSION['hot_water_use']='brak danych ';
    						$_SESSION['cold_water_use']='brak danych ';
    						$_SESSION['electricity_cost']='brak danych ';
    						$_SESSION['hot_water_cost']='brak danych ';
    						$_SESSION['cold_water_cost']='brak danych ';					
    						$_SESSION['total']='brak danych ';
    					}
    					
    					unset($_SESSION['error']);
    					$result ->free_result();
    					header('Location: kamienica.php');
                    }    
				}
				else
				{
					$_SESSION['error']='<span style="color:red">Nieprawidlowy login lub hasło</span>';
					header('Location: index.php');
				}
		
			}
			else
			{
				$_SESSION['error']='<span style="color:red">Nieprawidlowy login lub hasło</span>';
				
				header('Location: index.php');
			}
			
		}
		
		$connection->close();
	
	}

?>